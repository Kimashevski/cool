@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Create</h3></div>
                    <div class="panel-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Create Product</h3>
                                    <form enctype="multipart/form-data" action="{{route('createProduct')}}" method="POST">
                                        <label>Name</label><br>
                                        <input type="text" name="nameProduct"><br>
                                        <label>Title</label><br>
                                        <input type="text" name="titleProduct"><br>
                                        <label>Price</label><br>
                                        <input type="text" name="priceProduct"><br>
                                        <label>Choose image</label><br>
                                        <input type="file" name="image1">
                                        <br>
                                        <input type="file" name="image2">
                                        <br>
                                        <input type="file" name="image3">
                                        <br>
                                        <input type="file" name="image4">
                                        <br><label>Select category</label><br>
                                        <select id="category" name="listCategory">
                                            <option selected disabled>Select</option>
                                            @foreach($category as $value)
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                        </select><br>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"><br>
                                        <input type="submit" value="create">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
