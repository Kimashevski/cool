@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Update</h3></div>
                    <div class="panel-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Update Product</h3>
                                    <form enctype="multipart/form-data" action="{{route('updateProduct')}}" method="post">
                                        <label>Name</label><br>
                                        <input type="text" name="nameProduct" value="{{$product->name}}"><br>
                                        <label>Title</label><br>
                                        <input type="text" name="titleProduct" value="{{$product->title}}"><br>
                                        <label>Price</label><br>
                                        <input type="text" name="priceProduct" value="{{$product->price}}"><br>
                                        <br><label>Current images</label><br>
                                        @foreach($images as $valueImage)
                                            <img src="/images/{{$valueImage->src}}"style="width: 100px; height: 100px;">
                                            @endforeach
                                        <br><label>Choose new image</label><br>
                                        <input type="file" name="image1">
                                        <br>
                                        <input type="file" name="image2">
                                        <br>
                                        <input type="file" name="image3">
                                        <br>
                                        <input type="file" name="image4">
                                        <br><label>Select category</label><br>
                                        <select id="category" name="listCategory">
                                            <option selected disabled>Select</option>
                                            @foreach($category as $value)
                                                @if($product->category_id==$value->id)
                                                <option selected value="{{$value->id}}">{{$value->name}}</option>
                                                @else
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                  @endif
                                            @endforeach
                                        </select><br>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"><br>
                                        <input type="hidden" name="productId" value="{{$product->id}}"><br>
                                        <input type="submit" value="save">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
