@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

                <div class="panel panel-default">
                    <div class="panel-heading">Product</div>

                    <div class="panel-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>{{$product->name}}</h3>
                                    <h4>{{$product->title}}</h4>
                                    <h4>{{$product->price}}</h4>
                                    @foreach($image as $value)
                                        <img src="/images/{{$value->src}}" style="width: 200px;height: 200px;">
                                        @endforeach
                                </div>
                            </div>

                            <div class="row">
                                <label>Comments</label>
                                <div class="col-md-12">
                                    @foreach($comments as $valueComment)
                                        @foreach($users as $valueUser)
                                            @if($valueUser->id==$valueComment->user_id)
                                                @if(Auth::check()&&$valueUser->id==\Illuminate\Support\Facades\Auth::user()->id)
                                                    <h4>You</h4>
                                                    @else
                                                <h4>{{$valueUser->name}}</h4>
                                                    @endif
                                                @endif
                                            @endforeach
                                            <h5>{{$valueComment->text}}</h5>
                                            <h5>{{$valueComment->updated_at}}</h5>
                                        @endforeach

                                </div>

                                @if(Auth::check())
                                <form action="/admin/comment" method="post">
                                <div class="col-md-12">
                                    <p><textarea name="textComment"></textarea></p>
                                    <input type="hidden" name="productId" value="{{$product->id}}">
                                    <input type="submit" value="send">
                                </div>
                                </form>
                                    @endif
                            </div>


                        </div>
                    </div>
                </div>
        
        </div>
    </div>
@endsection
