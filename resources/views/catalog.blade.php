@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Catalog</div>

                    <div class="panel-body">
                        <h4>Welcome to Catalog!</h4><br>
                         <div class="container">
                             <div class="row">
                                 <div class="col-md-4">
                                     @foreach($data as $value)
                                         <h4>{{$value->name}}</h4>
                                         <h5>{{$value->title}}</h5>
                                         <h5>{{$value->price}}</h5>
                                         <a href="{{route('product',['id'=>$value->id])}}">More info>></a>
                                         @endforeach
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
