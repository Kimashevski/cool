@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Admin Panel</h3></div>
                    <div class="container">
                    <div class="panel-body">
                       <label>Products</label><br>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Title</th>
                                    <th>Price</th>
                                    <th>Category</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if($productData!=null)
                                 @foreach($productData as $value)
                                     <tr>
                                         <td>{{$value->name}}</td>
                                         <td>{{$value->title}}</td>
                                         <td>{{$value->price}}</td>

                                             @foreach($categoryData as $category)
                                                 @if($category->id==$value->category_id)
                                                     <td>{{$category->name}}</td>
                                                 @endif
                                             @endforeach
                                         <td><a href="{{route('deleteProduct',['id'=>$value->id])}}">Delete</a> <a href="/admin/update/{{$value->id}}">Update</a></td>
                                     </tr>
                                     @endforeach
                                 @endif
                                </tbody>
                            </table>

                        <label>Categories</label><br>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($categoryData!=null)
                                @foreach($categoryData as $value)
                                    <tr>
                                        <td>{{$value->name}}</td>
                                        <td><a href="{{route('deleteCategory',['id'=>$value->id])}}">Delete</a></td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <a href="{{route('createProduct')}}">Create new product</a><br>
                        <a href="{{route('createCategory')}}">Create new category</a><br>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
