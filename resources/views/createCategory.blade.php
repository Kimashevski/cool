@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Create</h3></div>
                    <div class="panel-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Create Category</h3>
                                    <form enctype="multipart/form-data" action="{{route('createCategory')}}" method="POST">
                                        <label>Name</label><br>
                                        <input type="text" name="nameCategory"><br>
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"><br>
                                        <input type="submit" value="create">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
