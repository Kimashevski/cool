<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/catalog','CatalogController@Show')->name('catalog');
Route::get('/product/{id}','CatalogController@showProduct')->name('product');

Route::get('/admin',['middleware'=>'auth','AdminController@Show'])->name('admin');
Route::get('/deleteProduct/{id}',['middleware'=>'auth','AdminController@deleteProduct'])->name('deleteProduct');
Route::get('/deleteCategory/{id}',['middleware'=>'auth','AdminController@deleteCategory'])->name('deleteCategory');

Route::get('/admin/createProduct',['middleware'=>'auth','AdminController@showCreate'])->name('createProduct');
Route::post('/admin/createProduct',['middleware'=>'auth','AdminController@createProduct'])->name('createProduct');

Route::get('/admin/createCategory',['middleware'=>'auth','AdminController@showCreateCategory'])->name('createCategory');
Route::post('/admin/createCategory',['middleware'=>'auth','AdminController@createCategory'])->name('createCategory');

Route::get('/admin/update/{id}',['middleware'=>'auth','AdminController@showUpdateProduct'])->name('showUpdateProduct');
Route::post('/admin/update',['middleware'=>'auth','AdminController@updateProduct'])->name('updateProduct');

Route::post('/admin/comment','CatalogController@sendComment');

