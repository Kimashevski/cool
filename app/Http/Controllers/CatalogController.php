<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Comment;
use App\Models\Image;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth;

class CatalogController extends Controller
{
    private function getAllProduct(){
        $product=Product::all();
        return $product;
    }
    private function getCategory(){
        $category=Category::all();
        return $category;
    }
    private function getImage(Product $product){
        $imageData=$product->getImages;
        return $imageData;
    }
    private function getComments(Product $product){
        $comments=$product->getComments;
        return $comments;
    }
    public function Show(){
         $data=$this->getAllProduct();
         $categories=$this->getCategory();
         return view('catalog',compact(['data','categories']));
    }

    public function showProduct($id){
        $product=Product::find($id);
        $image=$this->getImage($product);
        //$comments=$this->getComments($product);
        $comments=Comment::where('product_id',$id)->with('getUser')->get();
        $users=User::all();

        return view('product',compact(['product','image','comments','users']));
    }
    public function sendComment(Request $request){
        $user=\Illuminate\Support\Facades\Auth::user();
        $comment= new Comment([
            'text'=>$request->textComment,
            'user_id'=>$user->id,
            'product_id'=>$request->productId,
       ]);
        $comment->save();
        return $this->showProduct($request->productId);
    }

}
