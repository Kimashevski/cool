<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    private function getProduct(){
        $product=Product::all();
        return $product;
    }
    private function getCategory(){
        $category=Category::all();
        return $category;
    }

   public function Show(){
        $productData=$this->getProduct();
        $categoryData=$this->getCategory();
       return view('admin',compact(['productData','categoryData']));
   }
   public function deleteProduct($id){
       $product=Product::find($id);
       $product->getImages()->delete();
       $product->delete();
       return redirect('admin');
   }
   public function deleteCategory($id){
       $category=Category::where('id',$id)->with('getProducts.getImages')->first();
       foreach ($category['getProducts'] as $value){
           foreach ($value['getImages'] as $value1){
               $value1->delete();
           }
           $value->delete();
       }
       $category->delete();
       return redirect('admin');
   }
   public function showCreate(){
       $category=Category::all();
       return view('create',compact('category'));
   }
   public function createProduct(Request $request){
       if($request!=null){
           $category=Category::find($request->listCategory);
           $product=new Product(['name'=>$request->nameProduct,'title'=>$request->titleProduct,'price'=>$request->priceProduct]);
           $products=$category->getProducts()->save($product);
           for($i=1;$i<5;$i++)
           {
               if($request->hasFile('image'.$i))
               {
                   $image=$request->file('image'.$i);
                   $filename=$image->getClientOriginalName();
                   $serverFileName=time().'.'.$filename;
                   $path='images/';
                   $image->move($path,$serverFileName);
                   $imageNew=new Image(['src'=>$serverFileName]);
                   $imageNew=$products->getImages()->save($imageNew);
               }
           }
       }
       return redirect('admin');
   }
   public function showCreateCategory(){
       return view('createCategory');
   }
   public function createCategory(Request $request){
       if($request!=null){
           $category=new Category(['name'=>$request->nameCategory]);
           $category->save();
           return redirect('admin');
       }
   }
   public function showUpdateProduct($id){
       $product=Product::where('id',$id)->with('getCategory')->first();
       $images=$product->getImages;

       $category=Category::all();

       return view('update',compact(['product','category','images']));
   }
   public function updateProduct(Request $request){
         $product=Product::find($request->productId);
         $product->name=$request->nameProduct;
         $product->title=$request->titleProduct;
         $product->price=$request->priceProduct;
         $product->category_id=$request->listCategory;
           foreach ($product['getImages'] as $value1){
               $value1->delete();
           }
       for($i=1;$i<5;$i++)
       {
           if($request->hasFile('image'.$i))
           {
               $image=$request->file('image'.$i);
               $filename=$image->getClientOriginalName();
               $serverFileName=time().'.'.$filename;
               $path='images/';
               $image->move($path,$serverFileName);
               $imageNew=new Image(['src'=>$serverFileName]);
               $imageNew=$product->getImages()->save($imageNew);
           }
       }


         $product->save();
         return redirect('admin');
   }

}
