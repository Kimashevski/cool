<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=[
      'text','product_id','user_id',
        ];
    public function getUser(){
        return $this->belongsTo('App\User');
    }
    public function getProduct(){
        return $this->belongsTo('App\Models\Product');
    }
}
