<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable=[
        'src',
    ];
    public function getProduct(){
        return $this->belongsTo('App\Models\Product');
    }
}
