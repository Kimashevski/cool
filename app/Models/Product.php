<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=[
        'name','title','price',
    ];
    public function getCategory(){
        return $this->belongsTo('App\Models\Category');
    }
    public function getImages(){
        return $this->hasMany('App\Models\Image');
    }
}
